<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
    return redirect('/home');
});

Auth::routes(['register' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');

Route::get('/user/index',[App\Http\Controllers\User\UserController::class, 'index'])->name('user.index');
Route::get('/user/create',[App\Http\Controllers\User\UserController::class, 'create'])->name('user.create');
Route::post('/user/create/save',[App\Http\Controllers\User\UserController::class, 'save'])->name('user.create.save');
Route::get('/user/update/{id}',[App\Http\Controllers\User\UserController::class, 'update'])->name('user.update');
Route::post('/user/update/save',[App\Http\Controllers\User\UserController::class, 'save'])->name('user.update.save');
Route::post('/user/delete',[App\Http\Controllers\User\UserController::class, 'delete'])->name('user.delete');

Route::get('/user/profile',[App\Http\Controllers\User\ProfileController::class, 'index'])->name('user.profile.index');
Route::post('/user/profile/update',[App\Http\Controllers\User\ProfileController::class, 'update'])->name('user.profile.update');
Route::get('/user/change_password',[App\Http\Controllers\User\ProfileController::class, 'changePassword'])->name('user.change.password');
//Route::middleware(['throttle:save-password'])->group(function() {
//
//});
Route::post('/user/validate_password',[App\Http\Controllers\User\ProfileController::class, 'validateCurrentPassword'])->name('user.validate.password');
//Route::post('/user/save_password',[App\Http\Controllers\User\ProfileController::class, 'savePassword'])->name('user.save.password');

Route::get('/role/index',[App\Http\Controllers\Role\RoleController::class, 'index'])->name('role.index');
Route::get('/role/create',[App\Http\Controllers\Role\RoleController::class, 'create'])->name('role.create');
Route::post('/role/create/save',[App\Http\Controllers\Role\RoleController::class, 'save'])->name('role.create.save');
Route::get('/role/update/{id}',[App\Http\Controllers\Role\RoleController::class, 'update'])->name('role.update');
Route::post('/role/update/save',[App\Http\Controllers\Role\RoleController::class, 'save'])->name('role.update.save');
Route::post('/role/delete',[App\Http\Controllers\Role\RoleController::class, 'delete'])->name('role.delete');

Route::get('/position/index',[App\Http\Controllers\Position\PositionController::class, 'index'])->name('position.index');

Route::get('/leave-request/index',[App\Http\Controllers\LeaveRequest\LeaveRequestController::class, 'index'])->name('leave_request.index')->middleware('auth');
Route::get('/leave-request/create',[App\Http\Controllers\LeaveRequest\LeaveRequestController::class, 'create'])->name('leave_request.create');
Route::post('/leave-request/create/save',[App\Http\Controllers\LeaveRequest\LeaveRequestController::class, 'save'])->name('leave_request.create.save');
Route::get('/leave-request/update/{id}',[App\Http\Controllers\LeaveRequest\LeaveRequestController::class, 'update'])->name('leave_request.update');
Route::post('/leave-request/update/save',[App\Http\Controllers\LeaveRequest\LeaveRequestController::class, 'save'])->name('leave_request.update.save');
Route::get('/leave-request/review/{id}',[App\Http\Controllers\LeaveRequest\LeaveRequestController::class, 'review'])->name('leave_request.review');
Route::post('/leave-request/approval',[App\Http\Controllers\LeaveRequest\LeaveRequestController::class, 'approve'])->name('leave_request.approval');

Route::get('/leave-request/download-file',[App\Http\Controllers\LeaveRequest\LeaveRequestController::class, 'downloadFile'])->name('leave_request.download.file');

Route::get('/calendar/index',[App\Http\Controllers\Calendar\CalendarController::class, 'index'])->name('calendar.index');
Route::get('/calendar/event-source',[App\Http\Controllers\Calendar\CalendarController::class, 'getEventSource'])->name('calendar.event.source');
Route::post('/calendar/event-detail',[App\Http\Controllers\Calendar\CalendarController::class, 'getEventDetail'])->name('calendar.event.detail');
