<?php

return [

    'main_navigation'               => 'MAIN NAVIGATION',
    'blog'                          => 'Blog',
    'pages'                         => 'Pages',
    'account_settings'              => 'ACCOUNT SETTINGS',
    'profile'                       => 'Profile',
    'change_password'               => 'Change Password',
    'multilevel'                    => 'Multi Level',
    'level_one'                     => 'Level 1',
    'level_two'                     => 'Level 2',
    'level_three'                   => 'Level 3',
    'labels'                        => 'LABELS',
    'important'                     => 'Important',
    'warning'                       => 'Warning',
    'information'                   => 'Information',
    'users_management'              => 'USER MANAGEMENTS',
    'users'                         => 'Users',
    'roles'                         => 'Roles',
    'positions'                     => 'Positions',
    'al_management'                 => 'AL MANAGEMENTS',
    'al_request'                    => 'AL Request',
    'calendar'                      => 'Calendar',
    'personal_information'          => 'Personal Profile',
];
