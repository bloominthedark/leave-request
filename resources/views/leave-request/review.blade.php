@extends('adminlte::page')

@section('plugins.TempusDominusBs4', true)
@section('plugins.BsCustomFileInput', true)

@section('title', 'Review Request')

@section('content_header')
    <h1>Reviewing Request ID : {{$request->id}}</h1>
    <div class="row">
        <div class="col">

        </div>
        <div class="col" style="text-align: end">
            <span style="margin: 5px;">
                <x-adminlte-button type="button" id="download-file" label="Download Attachment" theme="info" icon="fas fa-lg fa-file-download"/>
            </span>
            <span style="margin: 5px">
                <a href="{{ route('leave_request.index') }}" class="btn btn-secondary">Back</a>
            </span>
        </div>
    </div>
@stop

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <form id="review-leave-request-form" class="custom-form" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col">
                        <x-adminlte-select name="request_type" label="Request Type" label-class="text-lightblue" igroup-size="md" disabled>
                            <x-slot name="prependSlot">
                                <div class="input-group-text text-lightblue">
                                    <i class="fas fa-id-card-alt"></i>
                                </div>
                            </x-slot>
                            @foreach($types as $type)
                                @if($type->code == $request->request_type)
                                    <option value="{{$type->code}}" selected>{{$type->name}}</option>
                                @endif
                            @endforeach
                        </x-adminlte-select>
                    </div>
                    <div class="col">
                        <x-adminlte-select name="manager_id" label="Direct Manager" label-class="text-lightblue" igroup-size="md" disabled>
                            <x-slot name="prependSlot">
                                <div class="input-group-text text-lightblue">
                                    <i class="fas fa-id-card-alt"></i>
                                </div>
                            </x-slot>
                            @foreach($managers as $manager)
                                @if($manager->id == $request->manager_id)
                                    <option value="{{$manager->id}}" selected>{{$manager->full_name}}</option>
                                @endif
                            @endforeach
                        </x-adminlte-select>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-adminlte-textarea name="reason" label="Leave Reason" rows=5 label-class="text-lightblue"
                                             igroup-size="sm" placeholder="Please enter your reason..." disabled>
                            {{$request->reason}}
                            <x-slot name="prependSlot">
                                <div class="input-group-text">
                                    <i class="fas fa-lg fa-file-alt text-lightblue"></i>
                                </div>
                            </x-slot>
                        </x-adminlte-textarea>
                    </div>
                    <div class="col">
                        <x-adminlte-textarea name="manager_cmt" label="Your Comment" rows=5 label-class="text-lightblue"
                                             igroup-size="sm" placeholder="Please enter what's your comment...">
                            <x-slot name="prependSlot">
                                <div class="input-group-text">
                                    <i class="fas fa-lg fa-file-alt text-lightblue"></i>
                                </div>
                            </x-slot>
                        </x-adminlte-textarea>
                    </div>
                </div>
                @php
                    $config = ['format' => 'YYYY-MM-DD'];
                @endphp
                <div class="row">
                    <div class="col">
                        <x-adminlte-input-date name="from_date" label="From Date" :config="$config" value="{{$request->from_date}}" disabled>
                            <x-slot name="appendSlot">
                                <div class="input-group-text text-lightblue">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                            </x-slot>
                        </x-adminlte-input-date>
                    </div>
                    <div class="col">
                        <x-adminlte-input-date name="to_date" label="To Date" :config="$config" value="{{$request->to_date}}" disabled>
                            <x-slot name="appendSlot">
                                <div class="input-group-text text-lightblue">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                            </x-slot>
                        </x-adminlte-input-date>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-adminlte-input-date name="submit_date" :config="$config" label="Submit Date" value="{{$request->submit_date}}" disabled>
                            <x-slot name="appendSlot">
                                <div class="input-group-text text-lightblue">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                            </x-slot>
                        </x-adminlte-input-date>
                    </div>
                    <div class="col">
                        @if($fileName)
                            <x-adminlte-input-file name="attachment_file" id="attachment_file" label="File Attachment" igroup-size="md" placeholder="{{$fileName}}" disabled>
                                <x-slot name="prependSlot">
                                    <div class="input-group-text text-lightblue">
                                        <i class="fas fa-upload"></i>
                                    </div>
                                </x-slot>
                            </x-adminlte-input-file>
                        @else
                            <x-adminlte-input-file name="attachment_file" id="attachment_file" label="File Attachment" igroup-size="md" placeholder="Not Include File Attachment" disabled>
                                <x-slot name="prependSlot">
                                    <div class="input-group-text text-lightblue">
                                        <i class="fas fa-upload"></i>
                                    </div>
                                </x-slot>
                            </x-adminlte-input-file>
                        @endif
                    </div>
                </div>
                <div class="d-flex justify-content-between">
                    <x-adminlte-button type="submit" name="approve" value="approve" label="Approve" theme="success" icon="far fa-lg fa-thumbs-up"/>
                    <x-adminlte-button type="submit" name="decline" value="decline" label="Decline" theme="danger" icon="far fa-lg fa-thumbs-down"/>
                </div>
            </form>
        </div>
        <div class="col-md-2"></div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/main-css.css">
@stop

@section('js')
    <script>
        jQuery(function ($) {
            $("form#review-leave-request-form button").click(function (event) {
                event.preventDefault();
                let formData = new FormData($("#review-leave-request-form")[0]);
                let requestId = '{{$request->id}}';
                formData.append('requestId',requestId);
                if ($(this).attr("value") == 'approve') {
                    formData.append('type','approve');
                } else if ($(this).attr("value") == 'decline') {
                    formData.append('type','decline');
                }
                let saveUrl = '{{ route('leave_request.approval') }}';
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url : saveUrl,
                    method : 'POST',
                    data : formData,
                    processData : false,
                    contentType : false,
                    success : function (data) {
                        if (data['status'] === true)
                        {
                            if (data['type'] === 'warning')
                            {
                                Swal.fire('Warning!', data['response'], 'warning');
                            }
                            if (data['type'] === 'success')
                            {
                                Swal.fire('Success!', data['response'], 'success');
                                setTimeout(function () {
                                    window.location.href = '{{ route('leave_request.index') }}';
                                },1500);
                            }
                        } else {
                            Swal.fire('Error!', data['response'], 'error');
                        }
                    },
                    error : function (data) {
                        Swal.fire('Error!', data['response'], 'error');
                        // setTimeout(function () {
                        //     window.location.reload();
                        // },2000);
                    }
                });
            });
            $("button#download-file").click(function (event) {
                event.preventDefault();
                let fileId = '{{$request->file_id}}';
                let downloadUrl = '{{route('leave_request.download.file')}}';
                let fileName = '{{$fileName}}';
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url : downloadUrl,
                    method: 'GET',
                    data: {
                        fileId : fileId
                    },
                    xhrFields: {
                        responseType: 'blob'
                    },
                    success : function (response) {
                        var blob = new Blob([response]);
                        console.log(response);
                        var link = document.createElement('a');
                        link.href = window.URL.createObjectURL(blob);
                        link.download = "{{$fileName}}";
                        link.click();
                    },
                    error : function (blob) {
                        console.log(blob);
                    }
                })
            })
        });
    </script>
@stop
