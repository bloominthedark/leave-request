@extends('adminlte::page')

@csrf

@section('title', 'Annual Leave Managements')

@section('content_header')
    <h1>Annual Leave Management</h1>
    <div class="row">
        <div class="col">

        </div>
        <div class="col">
            <span style="float: right">
                <a href="{{ route('leave_request.create') }}" class="btn btn-success">Create AL Request</a>
            </span>
        </div>
    </div>
@stop

@section('content')
    <div class="container-fluid">
        <table id="leave-request-table" class="table table-striped table-bordered table-hover">
            <thead class="thead-light">
            <tr>
                <th></th>
                <th>ID</th>
                <th>Owner</th>
                <th>Owner ID</th>
                <th>Type</th>
                <th>Status</th>
                <th>Manager</th>
                <th>Submit Date</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($requests))
                @foreach($requests as $request)
                    <tr>
                        <td></td>
                        <td>{{$request->id}}</td>
                        <td>{{$request->user_name}}</td>
                        <td>{{$request->user_id}}</td>
                        @foreach($types as $type)
                            @if($type->code == $request->request_type)
                                <td>{{$type->name}}</td>
                            @endif
                        @endforeach
                        @foreach($statuses as $status)
                            @if($status->code == $request->status)
                                @if($request->status == 'approve')
                                    <td class="approve-status">{{$status->name}}</td>
                                @elseif($request->status == 'decline')
                                    <td class="decline-status">{{$status->name}}</td>
                                @else
                                    <td class="pending-status">{{$status->name}}</td>
                                @endif
                            @endif
                        @endforeach
                        @foreach($managers as $manager)
                            @if($manager->id == $request->manager_id)
                                <td>{{$manager->full_name}}</td>
                            @endif
                        @endforeach
                        <td>{{$request->submit_date}}</td>
                        <td></td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="/css/main-css.css">
@stop

@section('js')
    <script src="{{ asset('js/sweetalert2/sweetalert2.min.js') }}"></script>
    <script>
        @if(session('permission'))
        jQuery(document).ready(async function () {
            const Toast = Swal.mixin({
                toast: true,
                position: 'bottom-right',
                iconColor: 'white',
                customClass: {
                    popup: 'colored-toast'
                },
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true
            })
            await Toast.fire({
                icon: 'error',
                title: '{{session('permission')}}'
            })
        });
        @endif
        jQuery(document).ready(function ($){
            var requestTable = $('#leave-request-table').DataTable({
                columnDefs: [
                    {
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<button title='edit' class='btn btn-outline-dark btn-align'>Edit</button><button title='review' class='btn btn-outline-success btn-align'>Review</button>"
                    },
                    {
                        "orderable": false,
                        "className": 'select-checkbox',
                        "targets": 0,
                    },
                    {
                        "targets": '_all',
                        "className": 'dt-center'
                    },
                    {
                        "targets": [3],
                        'visible': false,
                        'searchable': false
                    }
                ],
                "createdRow": function (row, data, dataIndex) {
                    if ( data[3] === '{{$currentUser->id}}') {
                        $(row).addClass('row-owner');
                    }
                },
                pagingType: 'full_numbers',
                stateSave: true,
                select: {
                    style : 'multi',
                    selector: 'td:first-child'
                },
                buttons: [
                    {
                        text   : 'Delete',
                        className : 'delete btn btn-outline-danger',
                        enabled : false,
                        action : function () {
                            let rowsData = requestTable.rows({selected: true}).data();
                            let bannersId = [];
                            for (let i=0; i < rowsData.length; i++)
                            {
                                bannersId.push(rowsData[i][1]);
                            }

                            Swal.fire({
                                title: 'Are You Sure You Want To Delete ?',
                                showDenyButton: false,
                                showCancelButton: true,
                                confirmButtonText: `Delete`,
                                customClass: {
                                    cancelButton: 'order-1 right-gap',
                                    confirmButton: 'order-2',
                                }
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    Swal.showLoading();
                                    $.ajax({
                                        url: '/admin/slider/banners/delete',
                                        method: 'POST',
                                        data: {
                                            id: bannersId
                                        },
                                        success : function (data) {
                                            if (data['status'] === true)
                                            {
                                                if (data['type'] === 'warning')
                                                {
                                                    Swal.fire('Warning!', data['response'], 'warning');
                                                }
                                                if (data['type'] === 'success')
                                                {
                                                    Swal.fire('Success!', data['response'], 'success');
                                                    setTimeout(function () {
                                                        window.location.reload();
                                                    },1500);
                                                }
                                            } else {
                                                Swal.fire('Error!', data['response'], 'error');
                                            }
                                        },
                                        error : function (data) {
                                            Swal.fire('Error!', data['response'], 'error');
                                            setTimeout(function () {
                                                window.location.reload();
                                            },2000);
                                        }
                                    });
                                }
                            });
                        }
                    },
                    {
                        text: 'Change Status',
                        className : 'change-status btn btn-outline-info',
                        enabled : false,
                        action: function ( e, dt, node, config ) {
                            let rowsData = requestTable.rows({selected: true}).data();
                            let bannersData = {};
                            for (let i=0; i < rowsData.length; i++)
                            {
                                bannersData[rowsData[i][1]] = rowsData[i][3];
                            }

                            Swal.fire({
                                title: 'Are You Sure You Want To Update ?',
                                showDenyButton: false,
                                showCancelButton: true,
                                confirmButtonText: `Update`,
                                customClass: {
                                    cancelButton: 'order-1 right-gap',
                                    confirmButton: 'order-2',
                                }
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    Swal.showLoading();
                                    $.ajax({
                                        url: '/admin/slider/banners/change_status',
                                        method: 'POST',
                                        data: {
                                            bannersData : bannersData
                                        },
                                        success : function (data) {
                                            if (data['status'] === true)
                                            {
                                                if (data['type'] === 'warning')
                                                {
                                                    Swal.fire('Warning!', data['response'], 'warning');
                                                }
                                                if (data['type'] === 'success')
                                                {
                                                    Swal.fire('Success!', data['response'], 'success');
                                                    setTimeout(function () {
                                                        window.location.reload();
                                                    },1500);
                                                }
                                            } else {
                                                Swal.fire('Error!', data['response'], 'error');
                                            }
                                        },
                                        error : function (data) {
                                            Swal.fire('Error!', data['response'], 'error');
                                            setTimeout(function () {
                                                window.location.reload();
                                            },2000);
                                        }
                                    });
                                }
                            });
                        }
                    }
                ]
            });

            // usersTable.on('select.dt deselect.dt',function () {
            //     usersTable.buttons('.delete').enable(
            //         usersTable.rows({selected: true}).indexes().length !== 0
            //     );
            //     usersTable.buttons('.change-status').enable(
            //         usersTable.rows({selected: true}).indexes().length !== 0
            //     )
            // });
            //
            // usersTable.buttons().container().appendTo( $('.col-sm-12:eq(0)', usersTable.table().container() ) );

            $('#leave-request-table tbody').on('click','button',function (event) {
                let action = this.title;
                let data = requestTable.row($(this).parents('tr')).data();
                let id = data[1];
                let requestOwnerId = data[3];
                let currentUserId = '{{$currentUser->id}}';
                let currentUserRole = '{{$currentUser->role}}';
                console.log(currentUserRole);
                if (action === 'edit')
                {
                    if (requestOwnerId === currentUserId) {
                        window.location.href = '/leave-request/update/' + id;
                    } else {
                        Swal.fire('Warning!', 'You Cannot Edit Request of Others User', 'warning');
                    }
                }
                if (action === 'review')
                {
                    if (currentUserRole === 'admin' || currentUserRole === 'man') {
                        window.location.href = '/leave-request/review/' + id;
                    } else {
                        Swal.fire('Warning!', 'You Do Not Have Permission To Approve Request. Please Wait Your Manager Review It', 'warning');
                    }
                }
            })
        });
    </script>
@stop
