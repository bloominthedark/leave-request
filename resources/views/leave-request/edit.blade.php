@extends('adminlte::page')

@section('plugins.TempusDominusBs4', true)
@section('plugins.BsCustomFileInput', true)

@section('title', 'Edit Leave Request')

@section('content_header')
    <h1>Edit Leave Request</h1>
    <div class="row">
        <div class="col">

        </div>
        <div class="col">
            <span style="float: right">
                <a href="{{ route('leave_request.index') }}" class="btn btn-info">Back</a>
            </span>
        </div>
    </div>
@stop

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <form id="edit-leave-request-form" class="custom-form" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col">
                        <x-adminlte-select name="request_type" label="Request Type" label-class="text-lightblue required" igroup-size="md" required>
                            <x-slot name="prependSlot">
                                <div class="input-group-text text-lightblue">
                                    <i class="fas fa-id-card-alt"></i>
                                </div>
                            </x-slot>
                            @foreach($types as $type)
                                @if($type->code == $request->request_type)
                                    <option value="{{$type->code}}" selected>{{$type->name}}</option>
                                @else
                                    <option value="{{$type->code}}">{{$type->name}}</option>
                                @endif
                            @endforeach
                        </x-adminlte-select>
                    </div>
                    <div class="col">
                        <x-adminlte-select name="manager_id" label="Your Direct Manager" label-class="text-lightblue required" igroup-size="md" required>
                            <x-slot name="prependSlot">
                                <div class="input-group-text text-lightblue">
                                    <i class="fas fa-id-card-alt"></i>
                                </div>
                            </x-slot>
                            @foreach($managers as $manager)
                                @if($manager->id == $request->manager_id)
                                    <option value="{{$manager->id}}" selected>{{$manager->full_name}}</option>
                                @else
                                    <option value="{{$manager->id}}" selected>{{$manager->full_name}}</option>
                                @endif
                            @endforeach
                        </x-adminlte-select>
                    </div>
                </div>
                <x-adminlte-textarea name="reason" label="Leave Reason" rows=5 label-class="text-lightblue"
                                     igroup-size="sm" placeholder="Please enter your reason...">
                    {{$request->reason}}
                    <x-slot name="prependSlot">
                        <div class="input-group-text">
                            <i class="fas fa-lg fa-file-alt text-lightblue"></i>
                        </div>
                    </x-slot>
                </x-adminlte-textarea>
                @php
                    $config = ['format' => 'YYYY-MM-DD'];
                @endphp
                <div class="row">
                    <div class="col">
                        <x-adminlte-input-date name="from_date" label="From Date" :config="$config" value="{{$request->from_date}}" placeholder="Choose an start leaving day..." autocomplete="off" label-class="required" required>
                            <x-slot name="appendSlot">
                                <div class="input-group-text text-lightblue">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                            </x-slot>
                        </x-adminlte-input-date>
                    </div>
                    <div class="col">
                        <x-adminlte-input-date name="to_date" label="To Date" :config="$config" value="{{$request->to_date}}" placeholder="Choose an end leaving day..." autocomplete="off" label-class="required" required>
                            <x-slot name="appendSlot">
                                <div class="input-group-text text-lightblue">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                            </x-slot>
                        </x-adminlte-input-date>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-adminlte-input-date name="submit_date" :config="$config" label="Submit Date" value="{{$request->submit_date}}" placeholder="Choose an submitted day..." autocomplete="off" label-class="required" required>
                            <x-slot name="appendSlot">
                                <div class="input-group-text text-lightblue">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                            </x-slot>
                        </x-adminlte-input-date>
                    </div>
                    <div class="col">
                        <x-adminlte-input-file name="attachment_file" id="attachment_file" label="File Attachment" igroup-size="md" placeholder="Choose a PDF file...">
                            <x-slot name="prependSlot">
                                <div class="input-group-text text-lightblue">
                                    <i class="fas fa-upload"></i>
                                </div>
                            </x-slot>
                        </x-adminlte-input-file>
                    </div>
                </div>
                <div class="d-flex justify-content-between">
                    <x-adminlte-button type="submit" label="Submit" theme="success" icon="fas fa-lg fa-save"/>
                    <x-adminlte-button type="reset" label="Reset" theme="secondary" icon="fas fa-lg fa-trash"/>
                </div>
            </form>
        </div>
        <div class="col-md-2"></div>
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/main-css.css">
@stop

@section('js')
    <script>
        jQuery(function ($) {
            $("form#edit-leave-request-form").submit(function (event) {
                event.preventDefault();
                let formData = new FormData(this);
                let requestId = '{{$request->id}}';
                let userId = '{{$user->id}}'
                let userName = '{{$user->full_name}}'
                let saveUrl = '{{ route('leave_request.update.save') }}';
                formData.append('request_id',requestId);
                formData.append('user_id',userId);
                formData.append('user_name',userName);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url : saveUrl,
                    method : 'POST',
                    data : formData,
                    processData : false,
                    contentType : false,
                    success : function (data) {
                        if (data['status'] === true)
                        {
                            if (data['type'] === 'warning')
                            {
                                Swal.fire('Warning!', data['response'], 'warning');
                            }
                            if (data['type'] === 'success')
                            {
                                Swal.fire('Success!', data['response'], 'success');
                                setTimeout(function () {
                                    window.location.href = '{{ route('leave_request.index') }}';
                                },1500);
                            }
                        } else {
                            Swal.fire('Error!', data['response'], 'error');
                        }
                    },
                    error : function (data) {
                        Swal.fire('Error!', data['response'], 'error');
                        setTimeout(function () {
                            window.location.reload();
                        },2000);
                    }
                });
            });
        });
    </script>
@stop
