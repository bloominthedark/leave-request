@extends('adminlte::page')

@csrf

@section('title', 'Application Calendar')

@section('content_header')
    <h1>Application Calendar</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div id='calendar'></div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="/css/main-css.css">
    <link rel="stylesheet" href="/js/fullcalendar-5.10.1/lib/main.min.css">
@stop

@section('js')
    <script src="{{ asset('js/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('js/fullcalendar-5.10.1/lib/main.min.js') }}"></script>
    <script src="{{ asset('js/fullcalendar-5.10.1/lib/locales-all.min.js') }}"></script>
    <script>
        jQuery(document).ready(function ($) {
            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                headerToolbar: {
                    start: 'today prev,next',
                    center: 'title',
                    end: 'dayGridMonth,timeGridWeek'
                },
                buttonText: {
                    today: 'Today',
                    month: 'Month',
                    week: 'Week',
                    day: 'Day',
                    list: 'list'
                },
                views: {

                },
                height: 750,
                contentHeight: 650,
                themeSystem: 'bootstrap',
                initialView: 'dayGridMonth',
                eventSources: [
                    {
                        url: "{{ route('calendar.event.source') }}",
                        color: 'green',
                        textColor: 'yellow',
                        failure: function () {
                            console.log('there was an error while fetching events!');
                        }
                    }
                ],
                eventClick: function (info) {
                    // alert('Event: ' + info.event.title + 'Event Id:' + info.event.id);
                    let eventId = info.event.id;
                    let getDetailUrl = '{{ route('calendar.event.detail') }}';
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: getDetailUrl,
                        method: 'POST',
                        data: {
                            eventId : eventId
                        },
                        success : function (data) {
                            if (data['status'] === true)
                            {
                                Swal.fire({
                                    title: '<strong>Event Detail</strong>',
                                    icon: 'info',
                                    html:'<div class="event-detail-text-wrapper"><span class="event-detail-inner-text"> User : <b>' + data['event_user'] + '</b></span>' +
                                        '<span class="event-detail-inner-text">Type : <b>' + data['event_type'] + '</b></span></div>' +
                                        '<div class="event-detail-text-wrapper"><span class="event-detail-inner-text">From : <b>' + data['event_from'] + '</b></span>' +
                                        '<span class="event-detail-inner-text">To : <b>' + data['event_to'] + '</b></span></div>' +
                                        '<div class="event-detail-text-wrapper"><span> Status : <b>' + data['event_status'] + '</b></span></div>'
                                });
                            } else {
                                Swal.fire('Error!', data['response'], 'error');
                            }
                        },
                        error : function (data) {
                            Swal.fire('Error!', data['response'], 'error');
                            console.log(data['message']);
                        }
                    });
                }
            });
            calendar.render();
        });
    </script>
@stop
