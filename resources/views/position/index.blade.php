@extends('adminlte::page')

@section('title', 'Positions Management')

@section('content_header')
    <h1>Positions Management</h1>
    <div class="row">
        <div class="col">

        </div>
        <div class="col">
            <span style="float: right">
                <a href="{{ route('user.create') }}" class="btn btn-info">Add Role</a>
            </span>
        </div>
    </div>
@stop

@section('content')
    <div class="container-fluid">
        <table id="positions-table" class="table table-striped table-bordered table-hover">
            <thead class="thead-light">
            <tr style="text-align: center">
                <th></th>
                <th>ID</th>
                <th>Code</th>
                <th>Label</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($positions))
                @foreach($positions as $position)
                    <tr style="text-align: center">
                        <td></td>
                        <td>{{$position->id}}</td>
                        <td>{{$position->code}}</td>
                        <td>{{$position->label}}</td>
                        <td></td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="/css/main-css.css">
@stop

@section('js')
    <script>
        jQuery(document).ready(function ($){
            var rolesTable = $('#positions-table').DataTable({
                columnDefs: [
                    {
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<button title='edit' class='btn btn-outline-dark btn-align'>Edit</button><button title='delete' class='btn btn-outline-danger btn-align'>Delete</button>"
                    },
                    {
                        "orderable": false,
                        "className": 'select-checkbox',
                        "targets": 0,
                    }
                ],
                pagingType: 'full_numbers',
                stateSave: true,
                select: {
                    style : 'multi',
                    selector: 'td:first-child'
                },
                buttons: [
                    {
                        text   : 'Delete',
                        className : 'delete btn btn-outline-danger',
                        enabled : false,
                        action : function () {
                            let rowsData = rolesTable.rows({selected: true}).data();
                            let bannersId = [];
                            for (let i=0; i < rowsData.length; i++)
                            {
                                bannersId.push(rowsData[i][1]);
                            }

                            Swal.fire({
                                title: 'Are You Sure You Want To Delete ?',
                                showDenyButton: false,
                                showCancelButton: true,
                                confirmButtonText: `Delete`,
                                customClass: {
                                    cancelButton: 'order-1 right-gap',
                                    confirmButton: 'order-2',
                                }
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    Swal.showLoading();
                                    $.ajax({
                                        url: '/admin/slider/banners/delete',
                                        method: 'POST',
                                        data: {
                                            id: bannersId
                                        },
                                        success : function (data) {
                                            if (data['status'] === true)
                                            {
                                                if (data['type'] === 'warning')
                                                {
                                                    Swal.fire('Warning!', data['response'], 'warning');
                                                }
                                                if (data['type'] === 'success')
                                                {
                                                    Swal.fire('Success!', data['response'], 'success');
                                                    setTimeout(function () {
                                                        window.location.reload();
                                                    },1500);
                                                }
                                            } else {
                                                Swal.fire('Error!', data['response'], 'error');
                                            }
                                        },
                                        error : function (data) {
                                            Swal.fire('Error!', data['response'], 'error');
                                            setTimeout(function () {
                                                window.location.reload();
                                            },2000);
                                        }
                                    });
                                }
                            });
                        }
                    },
                    {
                        text: 'Change Status',
                        className : 'change-status btn btn-outline-info',
                        enabled : false,
                        action: function ( e, dt, node, config ) {
                            let rowsData = rolesTable.rows({selected: true}).data();
                            let bannersData = {};
                            for (let i=0; i < rowsData.length; i++)
                            {
                                bannersData[rowsData[i][1]] = rowsData[i][3];
                            }

                            Swal.fire({
                                title: 'Are You Sure You Want To Update ?',
                                showDenyButton: false,
                                showCancelButton: true,
                                confirmButtonText: `Update`,
                                customClass: {
                                    cancelButton: 'order-1 right-gap',
                                    confirmButton: 'order-2',
                                }
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    Swal.showLoading();
                                    $.ajax({
                                        url: '/admin/slider/banners/change_status',
                                        method: 'POST',
                                        data: {
                                            bannersData : bannersData
                                        },
                                        success : function (data) {
                                            if (data['status'] === true)
                                            {
                                                if (data['type'] === 'warning')
                                                {
                                                    Swal.fire('Warning!', data['response'], 'warning');
                                                }
                                                if (data['type'] === 'success')
                                                {
                                                    Swal.fire('Success!', data['response'], 'success');
                                                    setTimeout(function () {
                                                        window.location.reload();
                                                    },1500);
                                                }
                                            } else {
                                                Swal.fire('Error!', data['response'], 'error');
                                            }
                                        },
                                        error : function (data) {
                                            Swal.fire('Error!', data['response'], 'error');
                                            setTimeout(function () {
                                                window.location.reload();
                                            },2000);
                                        }
                                    });
                                }
                            });
                        }
                    }
                ]
            });

            // usersTable.on('select.dt deselect.dt',function () {
            //     usersTable.buttons('.delete').enable(
            //         usersTable.rows({selected: true}).indexes().length !== 0
            //     );
            //     usersTable.buttons('.change-status').enable(
            //         usersTable.rows({selected: true}).indexes().length !== 0
            //     )
            // });
            //
            // usersTable.buttons().container().appendTo( $('.col-sm-12:eq(0)', usersTable.table().container() ) );

            $('#positions-table tbody').on('click','button',function () {
                let action = this.title;
                let data = rolesTable.row($(this).parents('tr')).data();
                let id = data[1];

                if (action === 'edit')
                {
                    window.location.href = '/user/update/' + id;
                }
                if (action === 'manage')
                {
                    window.location.href = '/admin/navigations/view/' + id;
                }
            })
        });
    </script>
@stop
