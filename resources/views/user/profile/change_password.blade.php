@extends('adminlte::auth.auth-page', ['auth_type' => 'login'])

@section('adminlte_css_pre')
    <link rel="stylesheet" href="{{ asset('vendor/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="/css/main-css.css">
@stop

@section('auth_header', __('adminlte::adminlte.change_password_message'))

@section('auth_body')
    <form id="user-change-password" action="login.html" method="post">
        @csrf
        <div class="input-group mb-3">
            <input type="password" name="current_pass" class="form-control" placeholder="Current Password" required>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
            </div>
        </div>
        <div class="input-group mb-3">
            <input type="password" id="new_pass" name="new_pass" class="form-control" placeholder="New Password" required>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
            </div>
        </div>
        <div class="input-group mb-3">
            <input type="password" id="retype_pass" name="retype_pass" class="form-control" placeholder="Retype New Password" required>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <button type="submit" class="btn btn-primary btn-block">Change Password</button>
            </div>
            <!-- /.col -->
        </div>
        <div class="row" style="margin-top: 10px">
            <div class="col-12">
                <a href="/user/profile" class="btn btn-secondary btn-block">Back</a>
            </div>
        </div>
    </form>
@stop

@section('auth_footer')

@stop

@section('js')
    <script>
        jQuery(document).ready(function ($) {
            $("form#user-change-password").submit(function (event) {
                event.preventDefault();
                let formData = new FormData(this);
                let submitUrl = '{{ route('user.validate.password') }}';
                let newPass = $("#new_pass").val();
                let retypePass = $('#retype_pass').val();
                if (newPass === retypePass) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url : submitUrl,
                        method : 'POST',
                        data : formData,
                        processData : false,
                        contentType : false,
                        success : function (data) {
                            if (data['status'] === true)
                            {
                                if (data['type'] === 'warning')
                                {
                                    Swal.fire('Warning!', data['response'], 'warning');
                                }
                                if (data['type'] === 'success')
                                {
                                    Swal.fire('Success!', data['response'], 'success');
                                    setTimeout(function () {
                                        window.location.href = '/login';
                                    },2000);
                                }
                            } else {
                                Swal.fire('Error!', data['response'], 'error');
                                setTimeout(function (){
                                    window.location.href = '/login';
                                },3000);
                            }
                        },
                        error : function (data) {
                            Swal.fire('Error!', data['response'], 'error');
                            // setTimeout(function () {
                            //     window.location.reload();
                            // },2000);
                        }
                    });
                } else {
                    Swal.fire('Warning!', 'Your Retype Password Does Not Match', 'warning');
                }
            })
        })
    </script>
@stop
