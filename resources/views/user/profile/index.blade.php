@extends('adminlte::page')

@csrf

@section('plugins.TempusDominusBs4', true)
@section('plugins.BsCustomFileInput', true)

@section('title', 'Personal Profile')

@section('content_header')
    <h1>Personal Profile</h1>
@stop

@section('content')
    <div class="container rounded bg-white mb-5">
        <div class="row">
            <div class="col-md-3 border-right">
                <div class="d-flex flex-column align-items-center text-center p-3 py-5"><img class="rounded-circle mt-5" width="150px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg"><span class="font-weight-bold">{{$user->full_name}}</span><span class="text-black-50">{{$userPosition}}</span><span> </span></div>
            </div>
            <div class="col-md-5 border-right">
                <form id="user-profile" method="POST" enctype="multipart/form-data">
                    <div class="p-3 py-5">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h4 class="text-right">Profile Settings</h4>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12"><label class="labels required">Full Name</label><input type="text" name="full_name" class="form-control" placeholder="full name" value="{{$user->full_name}}" required></div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12"><label class="labels">Phone Number</label><input type="text" name="phone" class="form-control" placeholder="enter phone number" value="{{$user->phone}}"></div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12"><label class="labels required">Email</label><input type="email" name="email" class="form-control" placeholder="enter email id" value="{{$user->email}}" required></div>
                        </div>
                        <div class="row mt-3">
                            @php
                                $config = ['format' => 'YYYY-MM-DD'];
                            @endphp
                            <div class="col-md-6">
                                <label class="labels">Birthday</label>
                                <x-adminlte-input-date name="birthday" :config="$config" placeholder="Choose a date..." value="{{$user->birthday}}" autocomplete="off">
                                    <x-slot name="appendSlot">
                                        <div class="input-group-text bg-gradient-danger">
                                            <i class="fas fa-lg fa-birthday-cake"></i>
                                        </div>
                                    </x-slot>
                                </x-adminlte-input-date>
                            </div>
                            <div class="col-md-6">
                                <label class="labels">Telegram ID</label>
                                <input type="text" name="telegram_id" class="form-control" value="{{$user->telegram_id}}" placeholder="account setting username">
                            </div>
                        </div>
                        <div class="mt-5 text-center"><button class="btn btn-success profile-button" type="submit">Update Profile</button></div>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
{{--                <div class="p-3 py-5">--}}
{{--                    <div class="d-flex justify-content-between align-items-center experience"><span>Edit Experience</span><span class="border px-3 p-1 add-experience"><i class="fa fa-plus"></i>&nbsp;Experience</span></div><br>--}}
{{--                    <div class="col-md-12"><label class="labels">Experience in Designing</label><input type="text" class="form-control" placeholder="experience" value=""></div> <br>--}}
{{--                    <div class="col-md-12"><label class="labels">Additional Details</label><input type="text" class="form-control" placeholder="additional details" value=""></div>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="/css/main-css.css">
@stop

@section('js')
    <script>
        jQuery(document).ready(function ($) {
            $("form#user-profile").submit(function (event) {
                event.preventDefault();
                let formData = new FormData(this);
                let updateUrl = '{{ route('user.profile.update') }}';
                let userId = '{{$user->id}}';
                formData.append('userId',userId)
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url : updateUrl,
                    method : 'POST',
                    data : formData,
                    processData : false,
                    contentType : false,
                    success : function (data) {
                        if (data['status'] === true)
                        {
                            if (data['type'] === 'warning')
                            {
                                Swal.fire('Warning!', data['response'], 'warning');
                            }
                            if (data['type'] === 'success')
                            {
                                Swal.fire('Success!', data['response'], 'success');
                                setTimeout(function () {
                                    window.location.href = '{{ route('user.profile.index') }}';
                                },1500);
                            }
                        } else {
                            Swal.fire('Error!', data['response'], 'error');
                        }
                    },
                    error : function (data) {
                        Swal.fire('Error!', data['response'], 'error');
                        setTimeout(function () {
                            window.location.reload();
                        },2000);
                    }
                });
            })
        })
    </script>
@stop
