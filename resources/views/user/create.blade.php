@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Create New User</h1>
    <div class="row">
        <div class="col">

        </div>
        <div class="col">
            <span style="float: right">
                <a href="{{ route('user.index') }}" class="btn btn-secondary">Back</a>
            </span>
        </div>
    </div>
@stop

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <form id="create-user-form" method="POST" enctype="multipart/form-data">
                @csrf
                <x-adminlte-input name="full_name" label="Full Name" placeholder="Please enter staff full name" label-class="text-lightblue required" required>
                    <x-slot name="prependSlot">
                        <div class="input-group-text">
                            <i class="fas fa-user text-lightblue"></i>
                        </div>
                    </x-slot>
                </x-adminlte-input>
                <x-adminlte-input type="email" name="email" label="Email" placeholder="Please enter staff email address. This is unique value." label-class="text-lightblue required" required>
                    <x-slot name="prependSlot">
                        <div class="input-group-text">
                            <i class="far fa-envelope text-lightblue"></i>
                        </div>
                    </x-slot>
                </x-adminlte-input>
                <x-adminlte-input name="password" label="Password" placeholder="Please enter password" label-class="text-lightblue required" required>
                    <x-slot name="prependSlot">
                        <div class="input-group-text">
                            <i class="fas fa-key text-lightblue"></i>
                        </div>
                    </x-slot>
                </x-adminlte-input>
                <x-adminlte-select name="position" label="Position" label-class="text-lightblue required" igroup-size="md" required>
                    <x-slot name="prependSlot">
                        <div class="input-group-text text-lightblue">
                            <i class="fas fa-id-card-alt"></i>
                        </div>
                    </x-slot>
                    <option value="">Please Select Staff Position</option>
                    @foreach($positions as $position)
                        <option value="{{$position->code}}">{{$position->label}}</option>
                    @endforeach
                </x-adminlte-select>
                <x-adminlte-select name="role" label="Role" label-class="text-lightblue required" igroup-size="md" required>
                    <x-slot name="prependSlot">
                        <div class="input-group-text text-lightblue">
                            <i class="fas fa-user-tag"></i>
                        </div>
                    </x-slot>
                    <option value="">Please Select Staff Role</option>
                    @foreach($roles as $role)
                        @if($user->role == 'admin')
                            <option value="{{$role->code}}">{{$role->label}}</option>
                        @else
                            @if($role->code != 'admin')
                                <option value="{{$role->code}}">{{$role->label}}</option>
                            @endif
                        @endif
                    @endforeach
                </x-adminlte-select>
                <div class="d-flex justify-content-between">
                    <x-adminlte-button type="submit" label="Submit" theme="success" icon="fas fa-lg fa-save"/>
                    <x-adminlte-button type="reset" label="Reset" theme="secondary" icon="fas fa-lg fa-trash"/>
                </div>
            </form>
        </div>
        <div class="col-md-2"></div>
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/main-css.css">
@stop

@section('js')
{{--    <script src="{{ asset('js/sweetalert2/sweetalert2.min.js') }}"></script>--}}
    <script>
        jQuery(function ($) {
            $("form#create-user-form").submit(function (event) {
                event.preventDefault();
                let formData = new FormData(this);
                let saveUrl = '{{ route('user.create.save') }}';
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url : saveUrl,
                    method : 'POST',
                    data : formData,
                    processData : false,
                    contentType : false,
                    success : function (data) {
                        if (data['status'] === true)
                        {
                            if (data['type'] === 'warning')
                            {
                                Swal.fire('Warning!', data['response'], 'warning');
                            }
                            if (data['type'] === 'success')
                            {
                                Swal.fire('Success!', data['response'], 'success');
                                setTimeout(function () {
                                    window.location.href = '{{ route('user.index') }}';
                                },1500);
                            }
                        } else {
                            Swal.fire('Error!', data['response'], 'error');
                        }
                    },
                    error : function (data) {
                        Swal.fire('Error!', data['response'], 'error');
                        setTimeout(function () {
                            window.location.reload();
                        },2000);
                    }
                });
            });
        });
    </script>
@stop
