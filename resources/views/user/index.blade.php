@extends('adminlte::page')

@csrf

@section('title', 'Users Management')

@section('content_header')
    <h1>Users Management</h1>
    <div class="row" style="height: 40px">
        @if($currentUser->role == 'admin' || $currentUser->role == 'man')
            <div class="col"></div>
            <div class="col">
            <span style="float: right">
                <a href="{{ route('user.create') }}" class="btn btn-success">Add User</a>
            </span>
            </div>
        @endif
    </div>
@stop

@section('content')
    <div class="container-fluid">
        <table id="users-table" class="table table-striped table-bordered table-hover">
            <thead class="thead-light">
            <tr style="text-align: center">
                <th></th>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Position</th>
                <th>Role</th>
                @if($currentUser->role == 'admin' || $currentUser->role == 'man')
                <th>Action</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @if(isset($users))
                @foreach($users as $user)
                    <tr style="text-align: center">
                        <td></td>
                        <td>{{$user['id']}}</td>
                        <td>{{$user['full_name']}}</td>
                        <td>{{$user['email']}}</td>
                        <td>{{$user['position']['label']}}</td>
                        <td>{{$user['role']['label']}}</td>
                        @if($currentUser->role == 'admin' || $currentUser->role == 'man')
                        <td></td>
                        @endif
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="/css/main-css.css">
@stop

@section('js')
    <script>
        @if(session('permission'))
        jQuery(document).ready(async function () {
            const Toast = Swal.mixin({
                toast: true,
                position: 'bottom-right',
                iconColor: 'white',
                customClass: {
                    popup: 'colored-toast'
                },
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true
            })
            await Toast.fire({
                icon: 'error',
                title: '{{session('permission')}}'
            })
        });
        @endif
        jQuery(document).ready(function ($){

            var usersTable = $('#users-table').DataTable({
                columnDefs: [
                    @if($currentUser->role == 'admin' || $currentUser->role == 'man')
                    {
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<button title='edit' class='btn btn-outline-dark btn-align'>Edit</button><button title='delete' class='btn btn-outline-danger btn-align'>Delete</button>"
                    },
                    @endif
                    {
                        "orderable": false,
                        "className": 'select-checkbox',
                        "targets": 0,
                    },
                    {
                        "targets": '_all',
                        "className": 'dt-center'
                    }
                ],
                pagingType: 'full_numbers',
                stateSave: true,
                select: {
                    style : 'multi',
                    selector: 'td:first-child'
                },
                buttons: [
                    {
                        text   : 'Delete',
                        className : 'delete btn btn-outline-danger',
                        enabled : false,
                        action : function () {
                            let currentUserRole = '{{$currentUser->role}}';
                            if (currentUserRole === 'admin' || currentUserRole === 'man') {
                                let rowsData = usersTable.rows({selected: true}).data();
                                let usersId = [];
                                for (let i=0; i < rowsData.length; i++)
                                {
                                    usersId.push(rowsData[i][1]);
                                }

                                Swal.fire({
                                    title: 'Are You Sure You Want To Delete ?',
                                    showDenyButton: false,
                                    showCancelButton: true,
                                    confirmButtonText: `Delete`,
                                    customClass: {
                                        cancelButton: 'order-1 right-gap',
                                        confirmButton: 'order-2',
                                    }
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        Swal.showLoading();
                                        $.ajax({
                                            headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            },
                                            url: '{{route('user.delete')}}',
                                            method: 'POST',
                                            data: {
                                                id: usersId
                                            },
                                            success : function (data) {
                                                if (data['status'] === true)
                                                {
                                                    if (data['type'] === 'warning')
                                                    {
                                                        Swal.fire('Warning!', data['response'], 'warning');
                                                    }
                                                    if (data['type'] === 'success')
                                                    {
                                                        Swal.fire('Success!', data['response'], 'success');
                                                        setTimeout(function () {
                                                            window.location.reload();
                                                        },1500);
                                                    }
                                                } else {
                                                    Swal.fire('Error!', data['response'], 'error');
                                                }
                                            },
                                            error : function (data) {
                                                Swal.fire('Error!', data['response'], 'error');
                                                setTimeout(function () {
                                                    window.location.reload();
                                                },2000);
                                            }
                                        });
                                    }
                                });
                            } else {
                                Swal.fire('Warning!', 'This Function Can Only Be Used By Administrators or Managers', 'warning');
                            }
                        }
                    }
                ]
            });

            usersTable.on('select.dt deselect.dt',function () {
                usersTable.buttons('.delete').enable(
                    usersTable.rows({selected: true}).indexes().length !== 0
                );
            });

            usersTable.buttons().container().appendTo( $('.col-sm-12:eq(0)', usersTable.table().container() ) );

            $('#users-table tbody').on('click','button',function (event) {
                let action = this.title;
                let data = usersTable.row($(this).parents('tr')).data();
                let id = data[1];
                let currentUserRole = '{{$currentUser->role}}';

                if (action === 'edit')
                {
                    if (currentUserRole === 'admin' || currentUserRole === 'man') {
                        window.location.href = '/user/update/' + id;
                    } else {
                        Swal.fire('Warning!', 'This Function Can Only Be Used By Administrators or Managers', 'warning');
                    }
                }
                if (action === 'delete')
                {
                    let deleteUrl = '{{ route('user.delete') }}';
                    Swal.fire({
                        title: 'Are You Sure You Want To Delete ?',
                        showCancelButton: true,
                        confirmButtonText: 'Delete',
                        customClass: {
                            cancelButton: 'order-1 right-gap',
                            confirmButton: 'order-2',
                        }
                    }).then((result) => {
                        if (result.isConfirmed) {
                            Swal.showLoading();
                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: deleteUrl,
                                method: 'POST',
                                data: {
                                    userId : id
                                },
                                success : function (data) {
                                    if (data['status'] === true)
                                    {
                                        if (data['type'] === 'warning')
                                        {
                                            Swal.fire('Warning!', data['response'], 'warning');
                                        }
                                        if (data['type'] === 'success')
                                        {
                                            Swal.fire('Success!', data['response'], 'success');
                                            setTimeout(function () {
                                                window.location.reload();
                                            },1500);
                                        }
                                    } else {
                                        Swal.fire('Error!', data['response'], 'error');
                                    }
                                },
                                error : function (data) {
                                    Swal.fire('Error!', data['response'], 'error');
                                    setTimeout(function () {
                                        window.location.reload();
                                    },2000);
                                }
                            });
                        }
                    });
                }
            })
        });
    </script>
@stop
