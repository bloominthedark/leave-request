@extends('adminlte::page')

@section('title', 'Roles Management')

@section('content_header')
    <h1>Roles Management</h1>
    <div class="row">
        @if($currentUser->role == 'admin' || $currentUser->role == 'man')
            <div class="col"></div>
            <div class="col">
            <span style="float: right">
                <a href="{{ route('role.create') }}" class="btn btn-success">Add Role</a>
            </span>
            </div>
        @endif
    </div>
@stop

@section('content')
    <div class="container-fluid">
        <table id="roles-table" class="table table-striped table-bordered table-hover">
            <thead class="thead-light">
            <tr style="text-align: center">
                <th></th>
                <th>ID</th>
                <th>Code</th>
                <th>Label</th>
                @if($currentUser->role == 'admin' || $currentUser->role == 'man')
                <th>Action</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @if(isset($roles))
                @foreach($roles as $role)
                    <tr style="text-align: center">
                        <td></td>
                        <td>{{$role->id}}</td>
                        <td>{{$role->code}}</td>
                        <td>{{$role->label}}</td>
                        @if($currentUser->role == 'admin' || $currentUser->role == 'man')
                        <td></td>
                        @endif
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="/css/main-css.css">
@stop

@section('js')
    <script>
        @if(session('permission'))
        jQuery(document).ready(async function () {
            const Toast = Swal.mixin({
                toast: true,
                position: 'bottom-right',
                iconColor: 'white',
                customClass: {
                    popup: 'colored-toast'
                },
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true
            })
            await Toast.fire({
                icon: 'error',
                title: '{{session('permission')}}'
            })
        });
        @endif
        jQuery(document).ready(function ($){
            var rolesTable = $('#roles-table').DataTable({
                columnDefs: [
                    @if($currentUser->role == 'admin' || $currentUser->role == 'man')
                    {
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<button title='edit' class='btn btn-outline-dark btn-align'>Edit</button><button title='delete' class='btn btn-outline-danger btn-align'>Delete</button>"
                    },
                    @endif
                    {
                        "orderable": false,
                        "className": 'select-checkbox',
                        "targets": 0,
                    }
                ],
                pagingType: 'full_numbers',
                stateSave: true,
                select: {
                    style : 'multi',
                    selector: 'td:first-child'
                },
                buttons: [
                    {
                        text   : 'Delete',
                        className : 'delete btn btn-outline-danger',
                        enabled : false,
                        action : function () {
                            let rowsData = rolesTable.rows({selected: true}).data();
                            let bannersId = [];
                            for (let i=0; i < rowsData.length; i++)
                            {
                                bannersId.push(rowsData[i][1]);
                            }

                            Swal.fire({
                                title: 'Are You Sure You Want To Delete ?',
                                showDenyButton: false,
                                showCancelButton: true,
                                confirmButtonText: `Delete`,
                                customClass: {
                                    cancelButton: 'order-1 right-gap',
                                    confirmButton: 'order-2',
                                }
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    Swal.showLoading();
                                    $.ajax({
                                        url: '/admin/slider/banners/delete',
                                        method: 'POST',
                                        data: {
                                            id: bannersId
                                        },
                                        success : function (data) {
                                            if (data['status'] === true)
                                            {
                                                if (data['type'] === 'warning')
                                                {
                                                    Swal.fire('Warning!', data['response'], 'warning');
                                                }
                                                if (data['type'] === 'success')
                                                {
                                                    Swal.fire('Success!', data['response'], 'success');
                                                    setTimeout(function () {
                                                        window.location.reload();
                                                    },1500);
                                                }
                                            } else {
                                                Swal.fire('Error!', data['response'], 'error');
                                            }
                                        },
                                        error : function (data) {
                                            Swal.fire('Error!', data['response'], 'error');
                                            setTimeout(function () {
                                                window.location.reload();
                                            },2000);
                                        }
                                    });
                                }
                            });
                        }
                    },
                    {
                        text: 'Change Status',
                        className : 'change-status btn btn-outline-info',
                        enabled : false,
                        action: function ( e, dt, node, config ) {
                            let rowsData = rolesTable.rows({selected: true}).data();
                            let bannersData = {};
                            for (let i=0; i < rowsData.length; i++)
                            {
                                bannersData[rowsData[i][1]] = rowsData[i][3];
                            }

                            Swal.fire({
                                title: 'Are You Sure You Want To Update ?',
                                showDenyButton: false,
                                showCancelButton: true,
                                confirmButtonText: `Update`,
                                customClass: {
                                    cancelButton: 'order-1 right-gap',
                                    confirmButton: 'order-2',
                                }
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    Swal.showLoading();
                                    $.ajax({
                                        url: '/admin/slider/banners/change_status',
                                        method: 'POST',
                                        data: {
                                            bannersData : bannersData
                                        },
                                        success : function (data) {
                                            if (data['status'] === true)
                                            {
                                                if (data['type'] === 'warning')
                                                {
                                                    Swal.fire('Warning!', data['response'], 'warning');
                                                }
                                                if (data['type'] === 'success')
                                                {
                                                    Swal.fire('Success!', data['response'], 'success');
                                                    setTimeout(function () {
                                                        window.location.reload();
                                                    },1500);
                                                }
                                            } else {
                                                Swal.fire('Error!', data['response'], 'error');
                                            }
                                        },
                                        error : function (data) {
                                            Swal.fire('Error!', data['response'], 'error');
                                            setTimeout(function () {
                                                window.location.reload();
                                            },2000);
                                        }
                                    });
                                }
                            });
                        }
                    }
                ]
            });

            // usersTable.on('select.dt deselect.dt',function () {
            //     usersTable.buttons('.delete').enable(
            //         usersTable.rows({selected: true}).indexes().length !== 0
            //     );
            //     usersTable.buttons('.change-status').enable(
            //         usersTable.rows({selected: true}).indexes().length !== 0
            //     )
            // });
            //
            // usersTable.buttons().container().appendTo( $('.col-sm-12:eq(0)', usersTable.table().container() ) );

            $('#roles-table tbody').on('click','button',function () {
                let action = this.title;
                let data = rolesTable.row($(this).parents('tr')).data();
                let id = data[1];
                let code = data[2];

                if (action === 'edit')
                {
                    window.location.href = '/role/update/' + code;
                }
                if (action === 'delete')
                {
                    let deleteUrl = '{{ route('role.delete') }}';
                    Swal.fire({
                        title: 'All User Has Belongs To This Role Will Be Set Is Undefined. You Need To Update These Users Manually After Deleting The Role.',
                        showCancelButton: true,
                        confirmButtonText: 'Delete',
                        customClass: {
                            cancelButton: 'order-1 right-gap',
                            confirmButton: 'order-2',
                        }
                    }).then((result) => {
                        if (result.isConfirmed) {
                            Swal.showLoading();
                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: deleteUrl,
                                method: 'POST',
                                data: {
                                    roleId : id,
                                    roleCode : code
                                },
                                success : function (data) {
                                    if (data['status'] === true)
                                    {
                                        if (data['type'] === 'warning')
                                        {
                                            Swal.fire('Warning!', data['response'], 'warning');
                                        }
                                        if (data['type'] === 'success')
                                        {
                                            Swal.fire('Success!', data['response'], 'success');
                                            setTimeout(function () {
                                                window.location.reload();
                                            },1500);
                                        }
                                    } else {
                                        Swal.fire('Error!', data['response'], 'error');
                                    }
                                },
                                error : function (data) {
                                    Swal.fire('Error!', data['response'], 'error');
                                    setTimeout(function () {
                                        window.location.reload();
                                    },2000);
                                }
                            });
                        }
                    });
                }
            })
        });
    </script>
@stop
