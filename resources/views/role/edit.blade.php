@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Create New User</h1>
    <div class="row">
        <div class="col">

        </div>
        <div class="col">
            <span style="float: right">
                <a href="{{ route('role.index') }}" class="btn btn-secondary">Back</a>
            </span>
        </div>
    </div>
@stop

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <form id="create-user-role-form" method="POST" enctype="multipart/form-data">
                @csrf
                <x-adminlte-input name="code" label="Role Identifier Code" placeholder="Ex: Manager is 'man', Employee is 'emp'..." value="{{$role->code}}" label-class="text-lightblue required" required>
                    <x-slot name="prependSlot">
                        <div class="input-group-text">
                            <i class="fas fa-user text-lightblue"></i>
                        </div>
                    </x-slot>
                </x-adminlte-input>
                <x-adminlte-input name="label" label="Role Label" placeholder="Please enter role label" value="{{$role->label}}" label-class="text-lightblue required" required>
                    <x-slot name="prependSlot">
                        <div class="input-group-text">
                            <i class="fas fa-user text-lightblue"></i>
                        </div>
                    </x-slot>
                </x-adminlte-input>
                <div class="d-flex justify-content-between">
                    <x-adminlte-button type="submit" label="Submit" theme="success" icon="fas fa-lg fa-save"/>
                    <x-adminlte-button type="reset" label="Reset" theme="secondary" icon="fas fa-lg fa-trash"/>
                </div>
            </form>
        </div>
        <div class="col-md-2"></div>
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/main-css.css">
@stop

@section('js')
    {{--    <script src="{{ asset('js/sweetalert2/sweetalert2.min.js') }}"></script>--}}
    <script>
        jQuery(function ($) {
            $("form#create-user-role-form").submit(function (event) {
                event.preventDefault();
                let formData = new FormData(this);
                let saveUrl = '{{ route('role.create.save') }}';
                let roleCode = '{{$role->code}}';
                formData.append('roleCode',roleCode);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url : saveUrl,
                    method : 'POST',
                    data : formData,
                    processData : false,
                    contentType : false,
                    success : function (data) {
                        if (data['status'] === true)
                        {
                            if (data['type'] === 'warning')
                            {
                                Swal.fire('Warning!', data['response'], 'warning');
                            }
                            if (data['type'] === 'success')
                            {
                                Swal.fire('Success!', data['response'], 'success');
                                setTimeout(function () {
                                    window.location.href = '{{ route('role.index') }}';
                                },1500);
                            }
                        } else {
                            Swal.fire('Error!', data['response'], 'error');
                        }
                    },
                    error : function (data) {
                        Swal.fire('Error!', data['response'], 'error');
                        setTimeout(function () {
                            window.location.reload();
                        },2000);
                    }
                });
            });
        });
    </script>
@stop
