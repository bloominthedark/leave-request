<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'full_name' => 'Admin Admin',
            'email' => 'admin@lte.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'position' => 'DEV',
            'role'     => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('users')->insert([
            'full_name' => 'Hai Luu',
            'email' => 'luungochai95@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('admin123'),
            'position' => 'DEV',
            'role'     => 'admin',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
