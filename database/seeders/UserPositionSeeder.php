<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserPositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('staff_position')->insert([
            'code' => 'PM',
            'label' => 'Project Manager',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('staff_position')->insert([
            'code' => 'BA',
            'label' => 'Business Analyst',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('staff_position')->insert([
            'code' => 'QC',
            'label' => 'Quality Control',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('staff_position')->insert([
            'code' => 'TL',
            'label' => 'Technical Lead',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('staff_position')->insert([
            'code' => 'DEV',
            'label' => 'Developer',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('staff_position')->insert([
            'code' => 'CEO',
            'label' => 'Chief Executive Officer',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('staff_position')->insert([
            'code' => 'CTO',
            'label' => 'Chief Technology Officer',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
