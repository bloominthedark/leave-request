<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RequestStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('request_status')->insert([
            'code'  => 'pending',
            'name' => 'Pending',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('request_status')->insert([
            'code'  => 'approve',
            'name' => 'Approve',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('request_status')->insert([
            'code'  => 'decline',
            'name' => 'Decline',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
