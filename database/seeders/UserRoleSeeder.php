<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'code'  => 'admin',
            'label' => 'Administrator',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('roles')->insert([
            'code'  => 'man',
            'label' => 'Manager',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('roles')->insert([
            'code'  => 'emp',
            'label' => 'Employee',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
