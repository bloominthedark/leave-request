<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AlTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('al_request_type')->insert([
            'code'  => 'annual',
            'name' => 'Annual Leave',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('al_request_type')->insert([
            'code'  => 'sick',
            'name' => 'Sick Leave',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('al_request_type')->insert([
            'code'  => 'personal',
            'name' => 'Personal Leave',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('al_request_type')->insert([
            'code'  => 'parental',
            'name' => 'Parental Leave',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('al_request_type')->insert([
            'code'  => 'non_pay',
            'name' => 'Leave Without Pay',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
