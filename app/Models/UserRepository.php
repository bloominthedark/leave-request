<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Role\Role;
use App\Models\Role\RoleRepository;
use App\Models\Position\Position;
use App\Models\Position\PositionRepository;

class UserRepository implements UserInterface
{
    protected $_role;

    protected $_roleRepository;

    protected $_position;

    protected $_positionRepository;

    public function __construct(
        Role $role,
        RoleRepository $roleRepository,
        Position $position,
        PositionRepository $positionRepository
    )
    {
        $this->_role = $role;
        $this->_roleRepository = $roleRepository;
        $this->_position = $position;
        $this->_positionRepository = $positionRepository;
    }

    public function loadUser($id)
    {
        $user = User::all()->where('id','=',$id)->first();
        if ($user) {
            return $user;
        }
        return null;
    }

    public function loadById($id)
    {
        $userObject = User::all()->where('id','=',$id)->first();
        if ($userObject) {
            $user = [
                'id' => $userObject->id,
                'full_name' => $userObject->full_name,
                'email' => $userObject->email,
                'position' => [
                    'code' => $userObject->position,
                    'label' => $this->_positionRepository->convertCodeToLabel($userObject->position)
                ],
                'role' => [
                    'code' => $userObject->role,
                    'label' => $this->_roleRepository->convertCodeToLabel($userObject->role)
                ]
            ];
            return $user;
        } else {
            return null;
        }
    }

    /**
     * Save New User Entity
     * @param $data
     * @return bool
     */
    public function create($data): bool
    {
        try {
            $data['password'] = Hash::make($data['password']);
            $userModel = new User();
            $userModel->fill($data);
            if ($userModel->save()) {
                return true;
            }
            return false;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public function update($userModel, $data): bool
    {
        try {
            $userModel->fill($data);
            if ($userModel->save()) {
                return true;
            }
            return false;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public function delete($users): bool
    {
        try {
            foreach ($users as $user) {
                $user->delete();
            }
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public function updatePassword($user,$newPassword): bool
    {
        try {
            $userData['password'] = Hash::make($newPassword);
            $user->fill($userData);
            if ($user->save()) {
                return true;
            }
            return false;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @return array|null
     */
    public function getUserCollection(): ?array
    {
        $userCollection = User::all();
        $users = [];
        foreach ($userCollection as $user) {
            $users[] = [
                'id' => $user->id,
                'full_name' => $user->full_name,
                'email' => $user->email,
                'position' => [
                    'code' => $user->position,
                    'label' => $this->_positionRepository->convertCodeToLabel($user->position)
                ],
                'role' => [
                    'code' => $user->role,
                    'label' => $this->_roleRepository->convertCodeToLabel($user->role)
                ],
            ];
        }
        if (!empty($users)) {
            return $users;
        }
        return null;
    }

    public function getManagerUser(): ?\Illuminate\Support\Collection
    {
        $managers = DB::table('users')->select(['id','full_name'])->where('role','=','man')->get();
        if (!empty($managers)) {
            return $managers;
        }
        return null;
    }

    public function getManagerLabel($userId)
    {
        $manager = DB::table('users')->select('full_name')->where('id','=',$userId)->get();
        if ($manager) {
            return $manager[0]->full_name;
        }
        return null;
    }
}
