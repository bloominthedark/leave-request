<?php

namespace App\Models;

interface UserInterface
{
    public function loadById($id);

    public function create($data);

    public function update($userModel,$data);

    public function delete(array $users);
}
