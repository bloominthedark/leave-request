<?php

namespace App\Models\Role;

use Illuminate\Support\Facades\DB;
use App\Models\Role\Role;

class RoleRepository implements RoleInterface
{
    public function loadByCode($code)
    {
        $role = Role::all()->where('code','=',$code)->first();
        if ($role) {
            return $role;
        } else {
            return null;
        }
    }

    public function create($data)
    {
        try {
            $roleModel = new Role();
            $roleModel->fill($data);
            if ($roleModel->save()) {
                return true;
            }
            return false;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public function update($roleModel, $data)
    {
        try {
            $roleModel->fill($data);
            if ($roleModel->save()) {
                return true;
            }
            return false;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public function delete($role)
    {
        try {
            $role->delete();
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     *
     * @return \Illuminate\Support\Collection|null
     */
    public function getRoleCollection()
    {
        $roles = DB::table('roles')->select(['code','label'])->get();
        if (!empty($roles)) {
            return $roles;
        }
        return null;
    }

    /**
     * Convert Role Code to Label
     * @param $_code
     * @return string|null
     */
    public function convertCodeToLabel($_code)
    {
        $roles = $this->getRoleCollection();
        $label = '';
        if ($roles) {
            foreach ($roles as $role) {
                if ($_code == $role->code) {
                    $label = $role->label;
                }
            }
            if ($label) {
                return $label;
            }
        }
        return $_code;
    }
}
