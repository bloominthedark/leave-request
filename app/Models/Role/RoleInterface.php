<?php

namespace App\Models\Role;

interface RoleInterface
{
    public function loadByCode($code);

    public function create($data);

    public function update($roleModel,$data);

    public function delete($role);
}
