<?php

namespace App\Models\Position;

use Illuminate\Support\Facades\DB;
use App\Models\Position\Position;

class PositionRepository implements PositionInterface
{

    public function loadById($id)
    {
        // TODO: Implement loadById() method.
    }

    public function create($data)
    {
        // TODO: Implement create() method.
    }

    public function update($positionModel, $data)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function getPositionCollection(): ?\Illuminate\Support\Collection
    {
        $positions = DB::table('staff_position')->select(['code','label'])->get();
        if (!empty($positions)) {
            return $positions;
        }
        return null;
    }

    public function convertCodeToLabel($_code)
    {
        $positions = $this->getPositionCollection();
        $label = '';
        if ($positions) {
            foreach ($positions as $position) {
                if ($_code == $position->code) {
                    $label = $position->label;
                }
            }
            if ($label) {
                return $label;
            }
        }
        return $_code;
    }
}
