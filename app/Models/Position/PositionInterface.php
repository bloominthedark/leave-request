<?php

namespace App\Models\Position;

interface PositionInterface
{
    public function loadById($id);

    public function create($data);

    public function update($positionModel,$data);

    public function delete($id);
}
