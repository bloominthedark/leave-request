<?php

namespace App\Models\LeaveRequest;

use Illuminate\Support\Facades\DB;
use App\Models\LeaveRequest\LeaveRequest;

class LeaveRequestRepository implements LeaveRequestInterface
{

    public function loadById($id)
    {
        $request = LeaveRequest::all()->where('id','=', $id)->first();
        if ($request) {
            return $request;
        }
        return null;
    }

    public function getRequest($id)
    {
        $request = LeaveRequest::all()->where('id','=',$id)->first();
        if ($request) {
            return $request;
        }
        return null;
    }

    public function create($data)
    {
        try {
            $request = new LeaveRequest();
            $request->fill($data);
            if ($request->save()) {
                return $request;
            }
            return false;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public function update($requestModel, $data)
    {
        try {
            $requestModel->fill($data);
            if ($requestModel->save()) {
                return $requestModel;
            }
            return false;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public function delete($id): bool
    {
        $request = $this->loadById($id);
        if ($request->delete()) {
            return true;
        }
        return false;
    }

    public function getLeaveRequestCollection()
    {
        $requests = LeaveRequest::all();
        if (!empty($requests)) {
            return $requests;
        }
        return null;
    }

    public function getRequestTypes(): ?\Illuminate\Support\Collection
    {
        $types = DB::table('al_request_type')->select(['code','name'])->get();
        if (!empty($types)) {
            return $types;
        }
        return null;
    }

    public function getRequestStatus(): ?\Illuminate\Support\Collection
    {
        $statuses = DB::table('request_status')->select(['code','name'])->get();
        if (!empty($statuses)) {
            return $statuses;
        }
        return null;
    }

    public function getRequestStatusLabel($statusCode): ?\Illuminate\Support\Collection
    {
        $status = DB::table('request_status')->select('name')->where('code','=',$statusCode)->get();
        if ($status) {
            return $status[0]->name;
        }
        return null;
    }

    public function getRequestTypeLabel($typeCode)
    {
        $type = DB::table('al_request_type')->select('name')->where('code','=',$typeCode)->get();
        if ($type) {
            return $type[0]->name;
        }
        return null;
    }

    public function getFileName($fileId)
    {
        $file = DB::table('file_uploads')->select('name')->where('id','=',$fileId)->get();
//        var_dump($file);
//        die();
        if ($file) {
            return $file[0]->name;
        }
        return null;
    }
}
