<?php

namespace App\Models\LeaveRequest;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveRequest extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'leave_request';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'user_name',
        'request_type',
        'reason',
        'from_date',
        'to_date',
        'submit_date',
        'manager_id',
        'status',
        'manager_cmt',
        'file_id'
    ];
}
