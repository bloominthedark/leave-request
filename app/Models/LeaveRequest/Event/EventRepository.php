<?php

namespace App\Models\LeaveRequest\Event;

use Illuminate\Support\Facades\DB;
use App\Models\LeaveRequest\LeaveRequest;
use App\Models\LeaveRequest\LeaveRequestRepository;

class EventRepository
{
    protected $leaveRequestRepository;

    public function __construct(LeaveRequestRepository $leaveRequestRepository)
    {
        $this->leaveRequestRepository = $leaveRequestRepository;
    }

    public function prepareEventSource($startDate, $endDate)
    {

    }

    public function getEventByDateRange($startDate, $endDate)
    {
        $requestEvents = DB::table('leave_request')->whereDate('from_date','>=',$startDate)->whereDate('to_date','<=',$endDate)->get();
        $events = [];
        foreach ($requestEvents as $requestEvent) {
            $events[] = [
                'id' => $requestEvent->id,
                'title' => $requestEvent->user_name.' - '.$this->leaveRequestRepository->getRequestTypeLabel($requestEvent->request_type),
                'start' => $requestEvent->from_date
            ];
        }
        return $events;
    }

    public function getEventDetail($eventId)
    {
        $event = DB::table('leave_request')->where('id','=',$eventId)->get();
        if ($event) {
            return $event[0];
        }
        return null;
    }

    public function getEventStatusLabel($statusCode)
    {
        $event = DB::table('request_status')->where('code','=',$statusCode)->select('name')->get();
        if ($event) {
            return $event[0]->name;
        }
        return null;
    }

    public function getEventTypeLabel($typeCode)
    {
        $type = DB::table('al_request_type')->select('name')->where('code','=',$typeCode)->get();
        if ($type) {
            return $type[0]->name;
        }
        return null;
    }
}
