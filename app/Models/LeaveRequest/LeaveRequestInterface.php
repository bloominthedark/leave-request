<?php

namespace App\Models\LeaveRequest;

interface LeaveRequestInterface
{
    public function loadById($id);

    public function create($data);

    public function update($requestModel,$data);

    public function delete($id);
}
