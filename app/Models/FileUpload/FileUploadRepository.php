<?php

namespace App\Models\FileUpload;

use App\Models\FileUpload\FileUpload;

class FileUploadRepository
{
    public function loadById($id)
    {
        $file = FileUpload::all()->where('id','=',$id)->first();
        if ($file) {
            return $file;
        }
        return null;
    }

    public function create($fileData)
    {
        try {
            $file = new FileUpload();
            $file->fill($fileData);
            if ($file->save()) {
                return $file;
            }
            return false;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public function update($fileModel,$fileData)
    {
        try {
            $fileModel->fill($fileData);
            if ($fileModel->save()) {
                return $fileModel;
            }
            return false;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public function getFileName($fileId)
    {
        $fileName = FileUpload::all()->where('id','=',$fileId)->get('name');
        if ($fileName) {
            return $fileName;
        }
        return null;
    }

    public function getFilePath($fileId)
    {
        $filePath = FileUpload::all()->where('id','=',$fileId)->get('path');
        if ($filePath) {
            return $filePath;
        }
        return null;
    }

    public function getFileDownloadPath()
    {

    }
}
