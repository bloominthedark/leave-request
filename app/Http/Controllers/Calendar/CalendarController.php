<?php

namespace App\Http\Controllers\Calendar;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Position\Position;
use App\Models\LeaveRequest\Event\EventRepository;
use App\Models\UserRepository;

class CalendarController extends Controller
{
    protected $eventRepository;

    protected $userRepository;

    public function __construct(EventRepository $eventRepository, UserRepository $userRepository)
    {
        $this->eventRepository = $eventRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $positions = Position::all();
        return view('calendar.index',['positions' => $positions]);
    }

    public function getEventSource(Request $request)
    {
        $data = $request->all();
        $startDate = $data['start'];
        $endDate = $data['end'];
        $startDate = substr($startDate,0,10);
        $endDate = substr($endDate,0,10);
        $events = $this->eventRepository->getEventByDateRange($startDate, $endDate);
        return response()->json($events);
    }

    public function getEventDetail(Request $request)
    {
        try {
            $postData = $request->post();
            $eventId = $postData['eventId'];
            $event = $this->eventRepository->getEventDetail($eventId);
            if ($event) {
                return [
                    'status'  =>  true,
                    'response'  =>  'Request Has Been Updated and re-sent.',
                    'event_user' => $event->user_name,
                    'event_type' => $this->eventRepository->getEventTypeLabel($event->request_type),
                    'event_from' => $event->from_date,
                    'event_to'   => $event->to_date,
                    'event_manager' => $this->userRepository->getManagerLabel($event->manager_id),
                    'event_status' => $this->eventRepository->getEventStatusLabel($event->status),
                ];
            } else {
                return [
                    'status'  =>  false,
                    'response'  =>  'Does not exists any information about this event.'
                ];
            }
        } catch (\Exception $exception) {
            return [
                'message' => $exception->getMessage(),
                'status' => false,
                'response' => 'An Error Occurred In Request Processing. Please Try Again.'
            ];
        }
    }
}
