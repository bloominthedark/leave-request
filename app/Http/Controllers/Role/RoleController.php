<?php

namespace App\Http\Controllers\Role;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Role\Role;
use App\Models\Role\RoleRepository;
use App\Models\UserRepository;

class RoleController extends Controller
{
    /**
     * @var UserRepository
     */
    protected UserRepository $_userRepository;

    /**
     * @var RoleRepository
     */
    protected RoleRepository $_roleRepository;

    /**
     * Create a new controller instance.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository,
        RoleRepository $roleRepository
    )
    {
        $this->middleware('auth');
        $this->_userRepository = $userRepository;
        $this->_roleRepository = $roleRepository;
    }

    public function index()
    {
        $roles = Role::all();
        $currentUser = Auth::user();
        return view('role.index',['roles' => $roles,'currentUser' => $currentUser]);
    }

    public function create()
    {
        $currentUser = Auth::user();
        $currentUserRole = $currentUser->role;
        if ($currentUserRole != 'admin' && $currentUserRole != 'man') {
            return redirect()->route('role.index')->with('permission','Permission Denied');
        }
        return view('role.create');
    }

    public function update($code)
    {
        $currentUser = Auth::user();
        $currentUserRole = $currentUser->role;
        if ($currentUserRole != 'admin' && $currentUserRole != 'man') {
            return redirect()->route('role.index')->with('permission','Permission Denied');
        }
        $role = $this->_roleRepository->loadByCode($code);
        return view('role.edit',['role' => $role]);
    }

    public function delete(Request $request)
    {
        $currentUser = Auth::user();
        $currentUserRole = $currentUser->role;
        if ($currentUserRole != 'admin' && $currentUserRole != 'man') {
            return redirect()->route('role.index')->with('permission','Permission Denied');
        }
        try {
            $postData = $request->post();
            if ($postData['roleCode'] == 'admin') {
                return [
                    'status' => true,
                    'type' => 'warning',
                    'response' => 'Can Not Delete System Administrator Role.'
                ];
            }
            $role = $this->_roleRepository->loadByCode($postData['roleCode']);
            if ($role) {
                $result = $this->_roleRepository->delete($role);
                if ($result) {
                    return [
                        'status' => true,
                        'type' => 'success',
                        'response' => 'User Role Has Been Delete.'
                    ];
                } else {
                    return [
                        'status' => false,
                        'response' => 'An Error Occurred While Deleting User Role. Please Try Again.'
                    ];
                }
            } else {
                return [
                    'status' => true,
                    'type' => 'warning',
                    'response' => 'User Role Not Found. Please Refresh Page.'
                ];
            }
        } catch (\Exception $exception) {
            return [
                'message' => $exception->getMessage(),
                'status' => false,
                'response' => 'An Error Occurred While Deleting User. Please Try Again.'
            ];
        }
    }

    public function save(Request $request)
    {
        try {
            $rolePostData = $request->post();
            if ( isset( $rolePostData['roleCode'] ) ) {
                $validator = Validator::make($rolePostData,[
                    'code' => 'unique:roles'
                ]);
                if ($validator->fails()) {
                    return [
                        'status' => true,
                        'type' => 'warning',
                        'response' => 'Role Identifier Is Already Exists.'
                    ];
                }
                $roleData = [];
                $roleData['code'] = $rolePostData['code'];
                $roleData['label'] = $rolePostData['label'];
                $role = $this->_roleRepository->loadByCode($rolePostData['roleCode']);
                if ($role) {
                    $result = $this->_roleRepository->update($role,$roleData);
                    if ($result) {
                        return [
                            'status' => true,
                            'type' => 'success',
                            'response' => 'User Role Has Been Updated.'
                        ];
                    } else {
                        return [
                            'status' => false,
                            'response' => 'An Error Occurred While Updating User Role Into Database. Please Try Again.'
                        ];
                    }
                } else {
                    return [
                        'status' => true,
                        'type' => 'warning',
                        'response' => 'User Not Found. Please Refresh Page.'
                    ];
                }
            } else {
                $validator = Validator::make($rolePostData,[
                    'code' => 'unique:roles'
                ]);
                if ($validator->fails())
                {
                    return [
                        'status'  =>  true,
                        'type'  =>  'warning',
                        'response'  =>  'Role Identifier Is Already Exists.'
                    ];
                }
                $roleData = [];
                $roleData['code'] = $rolePostData['code'];
                $roleData['label'] = $rolePostData['label'];

                $result = $this->_roleRepository->create($roleData);
                if ($result) {
                    return [
                        'status'  =>  true,
                        'type'  =>  'success',
                        'response'  =>  'User Role Has Been Created.'
                    ];
                } else {
                    return [
                        'status'  =>  false,
                        'response'  =>  'An Error Occurred While Saving User Role Into Database. Please Try Again.'
                    ];
                }
            }
        } catch (\Exception $exception) {
            return [
                'message' => $exception->getMessage(),
                'status' => false,
                'response' => 'An Error Occurred In Save Request. Please Try Again.'
            ];
        }
    }
}
