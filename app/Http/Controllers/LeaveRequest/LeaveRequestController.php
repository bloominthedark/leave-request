<?php

namespace App\Http\Controllers\LeaveRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewRequest;
use App\Notifications\UpdateRequest;
use App\Http\Controllers\Controller;
use App\Models\UserRepository;
use App\Models\LeaveRequest\LeaveRequestRepository;
use App\Models\FileUpload\FileUploadRepository;

class LeaveRequestController extends Controller
{
    protected $dataProvider;

    protected $userRepository;

    protected $leaveRequestRepository;

    protected $fileUploadRepository;

    protected $newRequestNotification;

    protected $updateRequestNotification;

    public function __construct(
        UserRepository $userRepository,
        LeaveRequestRepository $leaveRequestRepository,
        FileUploadRepository $fileUploadRepository,
        NewRequest $newRequestNotifications,
        UpdateRequest $updateRequestNotification
    )
    {
        $this->userRepository = $userRepository;
        $this->leaveRequestRepository = $leaveRequestRepository;
        $this->fileUploadRepository = $fileUploadRepository;
        $this->newRequestNotification = $newRequestNotifications;
        $this->updateRequestNotification = $updateRequestNotification;
    }

    /**
     * Show the Annual Leave Management Grid Page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $requests = $this->leaveRequestRepository->getLeaveRequestCollection();
        $types = $this->leaveRequestRepository->getRequestTypes();
        $statuses = $this->leaveRequestRepository->getRequestStatus();
        $managers = $this->userRepository->getManagerUser();
        $currentUser = Auth::user();
        return view('leave-request.index',['requests' => $requests, 'types' => $types, 'statuses' => $statuses, 'managers' => $managers, 'currentUser' => $currentUser]);
    }

    public function create()
    {
        $currentUser = Auth::user();
        $types = $this->leaveRequestRepository->getRequestTypes();
        $statuses = $this->leaveRequestRepository->getRequestStatus();
        $managers = $this->userRepository->getManagerUser();
        return view('leave-request.create',['user' => $currentUser, 'types' => $types, 'statuses' => $statuses, 'managers' => $managers]);
    }

    public function update($id)
    {
        $request = $this->leaveRequestRepository->getRequest($id);
        $currentUser = Auth::user();
        $types = $this->leaveRequestRepository->getRequestTypes();
        $statuses = $this->leaveRequestRepository->getRequestStatus();
        $managers = $this->userRepository->getManagerUser();
        return view('leave-request.edit',['request' => $request, 'user' => $currentUser, 'types' => $types, 'statuses' => $statuses, 'managers' => $managers]);
    }

    public function save(Request $request)
    {
        $currentUser = Auth::user();
        $currentUserRole = $currentUser->role;
        if ($currentUserRole != 'admin' && $currentUserRole != 'man') {
            return redirect()->route('leave_request.index')->with('permission','Permission Denied');
        }
        try {
            $requestData = $request->all();
            $saveFileFlag = false;
            if (isset($requestData['request_id'])) {
                $currentRequest = $this->leaveRequestRepository->loadById($requestData['request_id']);
                if ($currentRequest) {
                    if ($request->hasFile('attachment_file')) {
                        $currentFileId = $currentRequest->file_id;
                        $currentFile = $this->fileUploadRepository->loadById($currentFileId);
                        /* Remove old file in storage then set a new one */
                        $newFile = $request->file('attachment_file');
                        $fileExtension = $newFile->getClientOriginalExtension();
                        if ($fileExtension != 'pdf') {
                            return [
                                'status'  =>  true,
                                'type'  =>  'warning',
                                'response'  =>  'System Only Accepts Attachments File as PDF.'
                            ];
                        }
                        $newFileName = time().'_'.$newFile->getClientOriginalName();
                        $newFilePath = $newFile->storeAs('requests',$newFileName);
                        /* Set new file information for file data */
                        if ($newFilePath) {
                            $fileData['name'] = $newFileName;
                            $fileData['path'] = $newFilePath;
                            $updatedFile = $this->fileUploadRepository->update($currentFile, $fileData);
                            if ($updatedFile) {
                                $requestData['file_id'] = $updatedFile->id;
                                $saveFileFlag = true;
                            }
                        }
                    }
                    $result = $this->leaveRequestRepository->update($currentRequest,$requestData);
                    if ($result) {
                        $notifiable = $result->id;
                        Notification::send($notifiable, $this->newRequestNotification);
                        return [
                            'status'  =>  true,
                            'type'  =>  'success',
                            'response'  =>  'Request Has Been Updated and re-sent notification to manager.'
                        ];
                    } else {
                        return [
                            'status'  =>  false,
                            'response'  =>  'An Error Occurred While Updating Request Data Into Database. Please Try Again.'
                        ];
                    }
                }
                return [
                    'status' => true,
                    'type' => 'warning',
                    'response' => 'Leave Request Not Found. Please Refresh Page.'
                ];
            } else {
                if ($request->hasFile('attachment_file')) {
                    $file = $request->file('attachment_file');
                    $fileExtension = $file->getClientOriginalExtension();
                    if ($fileExtension != 'pdf') {
                        return [
                            'status'  =>  true,
                            'type'  =>  'warning',
                            'response'  =>  'System Only Accepts Attachments File as PDF.'
                        ];
                    }
                    /* Put file into storage and set file information for request data */
                    $fileName = time().'_'.$file->getClientOriginalName();
                    $filePath = $file->storeAs('requests',$fileName);
                    if ($filePath) {
                        $fileData['name'] = $fileName;
                        $fileData['path'] = $filePath;
                        $savedFile = $this->fileUploadRepository->create($fileData);
                        if ($savedFile) {
                            $requestData['file_id'] = $savedFile->id;
                            $requestData['file_path'] = $filePath;
                        }
                    }
                }
                $requestData['status'] = 'pending';
                $result = $this->leaveRequestRepository->create($requestData);
                if ($result) {
                    $notifiable = $result->id;
                    Notification::send($notifiable, $this->newRequestNotification);
                    return [
                        'status'  =>  true,
                        'type'  =>  'success',
                        'response'  =>  'Your Request Has Been Sent.'
                    ];
                } else {
                    return [
                        'status'  =>  false,
                        'response'  =>  'An Error Occurred While Saving Request Data Into Database. Please Try Again.'
                    ];
                }
            }
        } catch (\Exception $exception) {
            return [
                'message' => $exception->getMessage(),
                'status' => false,
                'response' => 'An Error Occurred In Request Processing. Please Try Again.'
            ];
        }
    }

    public function delete($id)
    {
        try {
            $this->leaveRequestRepository->delete($id);
        } catch (\Exception $exception) {
            return [
                'message' => $exception->getMessage(),
                'status' => false,
                'response' => 'An Error Occurred While Deleting Request. Please Try Again.'
            ];
        }
    }

    public function review($id)
    {
        $request = $this->leaveRequestRepository->getRequest($id);
        $types = $this->leaveRequestRepository->getRequestTypes();
        $statuses = $this->leaveRequestRepository->getRequestStatus();
        $managers = $this->userRepository->getManagerUser();
        if ($request->file_id) {
            $fileName = $this->leaveRequestRepository->getFileName($request->file_id);
        } else {
            $fileName = null;
        }
        return view('leave-request.review',['request' => $request, 'types' => $types, 'statuses' => $statuses, 'managers' => $managers, 'fileName' => $fileName]);
    }

    public function approve(Request $request)
    {
        try {
            $data = $request->post();
            if ($data) {
                $leaveRequest = $this->leaveRequestRepository->getRequest($data['requestId']);
                if ($leaveRequest) {
                    $data['status'] = $data['type'];
                    $result = $this->leaveRequestRepository->update($leaveRequest,$data);
                    if ($result) {
                        $notifiable = $result->id;
                        Notification::send($notifiable, $this->updateRequestNotification);
                        return [
                            'status'  =>  true,
                            'type'  =>  'success',
                            'response'  =>  'Request Status Has Been Updated.'
                        ];
                    } else {
                        return [
                            'status'  =>  false,
                            'response'  =>  'An Error Occurred While Updating Request Data Into Database. Please Try Again.'
                        ];
                    }
                }
            }
        } catch (\Exception $exception) {
            return [
                'message' => $exception->getMessage(),
                'status' => false,
                'response' => 'An Error Occurred In Request Processing. Please Try Again.'
            ];
        }
    }

    public function downloadFile(Request $request)
    {
        $postData = $request->post();
        $fileId = $postData['fileId'];
        $headers = ['Content-Type: application/pdf'];
        $file = $this->fileUploadRepository->loadById($fileId);
        $fileName = $file->name;
        $filePath = $file->path;
        return Storage::download($filePath,$fileName,$headers);
    }
}
