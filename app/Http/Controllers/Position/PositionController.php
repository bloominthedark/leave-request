<?php

namespace App\Http\Controllers\Position;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Position\Position;

class PositionController extends Controller
{
    public function index()
    {
        $positions = Position::all();
        return view('position.index',['positions' => $positions]);
    }
}
