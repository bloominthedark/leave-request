<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\UserRepository;
use App\Models\Role\RoleRepository;
use App\Models\Position\PositionRepository;


class ProfileController extends Controller
{
    /**
     * @var UserRepository
     */
    protected UserRepository $_userRepository;

    /**
     * @var RoleRepository
     */
    protected RoleRepository $_roleRepository;

    /**
     * @var PositionRepository
     */
    protected PositionRepository $_positionRepository;

    /**
     * Create a new controller instance.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository,
        RoleRepository $roleRepository,
        PositionRepository $positionRepository
    )
    {
        $this->middleware('auth');
        $this->_userRepository = $userRepository;
        $this->_roleRepository = $roleRepository;
        $this->_positionRepository = $positionRepository;
    }

    /**
     * Show the Users Management Grid Page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $userPosition = $this->_positionRepository->convertCodeToLabel($user->position);
        return view('user.profile.index', ['user' => $user,'userPosition' => $userPosition]);
    }

    public function update(Request $request)
    {
        try {
            $postData = $request->post();
            $currentUser = Auth::user();
            $requestedUserId = $postData['userId'];
            if ($requestedUserId != $currentUser->getAuthIdentifier()) {
                return [
                    'status' => true,
                    'type' => 'warning',
                    'response' => 'Requested Account Profile Does Not Match Your Current Logged In Account.'
                ];
            }
            $user = $this->_userRepository->loadUser($requestedUserId);
            if ($user) {
                $userData = [
                    'full_name' => $postData['full_name'],
                    'phone' => $postData['phone'],
                    'email' => $postData['email'],
                    'birthday' => $postData['birthday'],
                    'telegram_id' => $postData['telegram_id']
                ];
                $result = $this->_userRepository->update($user,$userData);
                if ($result) {
                    return [
                        'status' => true,
                        'type' => 'success',
                        'response' => 'Your Profile Information Has Been Updated.'
                    ];
                } else {
                    return [
                        'status' => false,
                        'response' => 'Something Went Wrong. Please Double Check Input Data and Try Again.'
                    ];
                }
            } else {
                return [
                    'status' => true,
                    'type' => 'warning',
                    'response' => 'User Not Found. Please Refresh Page.'
                ];
            }
        } catch (\Exception $exception) {
            return [
                'message' => $exception->getMessage(),
                'status' => false,
                'response' => 'An Error Occurred In Save Request. Please Try Again.'
            ];
        }
    }

    public function changePassword()
    {
        return view('user.profile.change_password');
    }

    public function validateCurrentPassword(Request $request)
    {
        $authUser = Auth::user();
        $postData = $request->post();
        if (RateLimiter::tooManyAttempts('save-password:'.$authUser->getAuthIdentifier(),$perMinute = 3)) {
            Auth::logout();
            $request->session()->invalidate();
            $request->session()->regenerateToken();
            return [
                'status' => false,
                'response' => 'You Have Reached The Allowed Limit. You Will Be Force Logged Out After 3 Seconds.'
            ];
        }
        if (Hash::check($postData['current_pass'],$authUser->getAuthPassword())) {
            $result = $this->_userRepository->updatePassword($authUser,$postData['new_pass']);
            if ($result) {
                Auth::logout();
                $request->session()->invalidate();
                $request->session()->regenerateToken();
                return [
                    'status' => true,
                    'type' => 'success',
                    'response' => 'Your Password Has Been Changed. Please Login In Again With New Password.'
                ];
            } else {
                return [
                    'status' => true,
                    'type' => 'warning',
                    'response' => 'Something Went Wrong, Please Refresh Page and Try Again Later.'
                ];
            }
        } else {
            if (RateLimiter::retriesLeft('save-password:'.$authUser->getAuthIdentifier(),$perMinute = 3)) {
                $timesLeft = RateLimiter::retriesLeft('save-password:'.$authUser->getAuthIdentifier(),$perMinute = 3);
                RateLimiter::hit('save-password:'.$authUser->getAuthIdentifier());
                return [
                    'status' => true,
                    'type' => 'warning',
                    'response' => 'The Current Password Has Entered Does Not Match.<br> You Have <b><span style="color: red">'.$timesLeft.'</span></b> Times Tries Left.'
                ];
            }
        }
    }
}
