<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserRepository;
use App\Models\Role\Role;
use App\Models\Role\RoleRepository;
use App\Models\Position\Position;


class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected UserRepository $_userRepository;

    /**
     * @var RoleRepository
     */
    protected RoleRepository $_roleRepository;

    /**
     * Create a new controller instance.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository,
        RoleRepository $roleRepository
    )
    {
        $this->middleware('auth');
        $this->_userRepository = $userRepository;
        $this->_roleRepository = $roleRepository;
    }

    /**
     * Show the Users Management Grid Page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = $this->_userRepository->getUserCollection();
        $currentUser = Auth::user();
        return view('user.index',['users' => $users, 'currentUser' => $currentUser]);
    }

    /**
     * Show the User Create Form Page
     */
    public function create()
    {
        $currentUser = Auth::user();
        $currentUserRole = $currentUser->role;
        if ($currentUserRole != 'admin' && $currentUserRole != 'man') {
            return redirect()->route('user.index')->with('permission','Permission Denied');
        }
        $roles = Role::all();
        $positions = Position::all();
        return view('user.create',['roles' => $roles, 'positions' => $positions, 'user' => $currentUser]);
    }

    public function read()
    {

    }

    public function update($id)
    {
        $currentUser = Auth::user();
        $currentUserRole = $currentUser->role;
        if ($currentUserRole != 'admin' && $currentUserRole != 'man') {
            return redirect()->route('user.index')->with('permission','Permission Denied');
        }
        $user = $this->_userRepository->loadById($id);
        $roles = Role::all();
        $positions = Position::all();
        return view('user.edit',['user' => $user, 'roles' => $roles, 'positions' => $positions]);
    }

    public function delete(Request $request)
    {
        $currentUser = Auth::user();
        $currentUserRole = $currentUser->role;
        if ($currentUserRole != 'admin' && $currentUserRole != 'man') {
            return redirect()->route('user.index')->with('permission','Permission Denied');
        }
        try {
            $postData = $request->post();
            $usersId = $postData['id'];
            $users = [];
            $errors = [];
            foreach ($usersId as $userId) {
                $userEntity = $this->_userRepository->loadUser($userId);
                if ($userEntity) {
                    $users[] = $userEntity;
                } else {
                    $errors[] = 'User Id: '.$userId.' Not Found';
                }
            }
            if (!empty($users)) {
                $result = $this->_userRepository->delete($users);
                if ($result) {
                    return [
                        'status' => true,
                        'type' => 'success',
                        'response' => 'These Users Has Been Deleted.'
                    ];
                } else {
                    return [
                        'status' => false,
                        'response' => 'An Error Occurred While Deleting Users. Please Try Again.'
                    ];
                }
            }
            $notFoundIds = implode(',',$errors);
            return [
                'status' => false,
                'response' => 'Cannot Find Any Users With These Ids : '.$notFoundIds.' Please Refresh Page and Try Again.'
            ];
        } catch (\Exception $exception) {
            return [
                'message' => $exception->getMessage(),
                'status' => false,
                'response' => 'An Error Occurred While Deleting User. Please Try Again.'
            ];
        }
    }

    /**
     * Handle Create New or Update User Entity
     */
    public function save(Request $request)
    {
        try {
            $userPostData = $request->post();
            if ( isset( $userPostData['userId'] ) ) {
                $userData = [];
                $userData['full_name'] = $userPostData['full_name'];
                $userData['position'] = $userPostData['position'];
                $userData['role'] = $userPostData['role'];

                $user = $this->_userRepository->loadUser($userPostData['userId']);
                if ($user) {
                    $result = $this->_userRepository->update($user,$userData);
                    if ($result) {
                        return [
                            'status' => true,
                            'type' => 'success',
                            'response' => 'User Has Been Updated.'
                        ];
                    } else {
                        return [
                            'status' => false,
                            'response' => 'An Error Occurred While Updating User Data Into Database. Please Try Again.'
                        ];
                    }
                } else {
                    return [
                        'status' => true,
                        'type' => 'warning',
                        'response' => 'User Not Found. Please Refresh Page.'
                    ];
                }
            } else {
                $validator = Validator::make($userPostData,[
                    'email' => 'unique:users'
                ]);
                if ($validator->fails())
                {
                    return [
                        'status'  =>  true,
                        'type'  =>  'warning',
                        'response'  =>  'Email Address Is Already Registered.'
                    ];
                }
                $userData = [];
                $userData['full_name'] = $userPostData['full_name'];
                $userData['email'] = $userPostData['email'];
                $userData['password'] = $userPostData['password'];
                $userData['position'] = $userPostData['position'];
                $userData['role'] = $userPostData['role'];

                $result = $this->_userRepository->create($userData);
                if ($result) {
                    return [
                        'status'  =>  true,
                        'type'  =>  'success',
                        'response'  =>  'User Has Been Created.'
                    ];
                } else {
                    return [
                        'status'  =>  false,
                        'response'  =>  'An Error Occurred While Saving User Data Into Database. Please Try Again.'
                    ];
                }
            }
        } catch (\Exception $exception) {
            return [
                'message' => $exception->getMessage(),
                'status' => false,
                'response' => 'An Error Occurred In Save Request. Please Try Again.'
            ];
        }
    }
}
