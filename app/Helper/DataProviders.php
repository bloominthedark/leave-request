<?php

namespace App\Helper;

use Illuminate\Support\Facades\DB;

class DataProviders
{
    public function getRequestTypes(): ?\Illuminate\Support\Collection
    {
        $types = DB::table('al_request_type')->select(['code','name'])->get();
        if (!empty($types)) {
            return $types;
        }
        return null;
    }

    public function getRequestStatus(): ?\Illuminate\Support\Collection
    {
        $statuses = DB::table('request_status')->select(['code','name'])->get();
        if (!empty($statuses)) {
            return $statuses;
        }
        return null;
    }

    public function getRequestStatusLabel($statusCode): ?\Illuminate\Support\Collection
    {
        $status = DB::table('request_status')->select('name')->where('code','=',$statusCode)->get();
        if ($status) {
            return $status;
        }
        return null;
    }

    public function getRequestTypeLabel($typeCode): ?\Illuminate\Support\Collection
    {
        $type = DB::table('al_request_type')->select('name')->where('code','=',$typeCode)->get();
        if ($type) {
            return $type;
        }
        return null;
    }
}
