<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use App\Models\LeaveRequest\LeaveRequestRepository;
use App\Models\UserRepository;

class NewRequest extends Notification
{
    use Queueable;

    protected LeaveRequestRepository $requestRepository;

    protected UserRepository $userRepository;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        LeaveRequestRepository $leaveRequestRepository,
        UserRepository $userRepository
    )
    {
        $this->requestRepository = $leaveRequestRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
//        return [TelegramChannel::class];
        return ["telegram"];
    }

    /**
     * Pass $notifiable to this method as an id of request object
     * Use the id to retrieve request object data in this method
     */
    public function toTelegram($notifiable)
    {
        $requestId = $notifiable;
        $request = $this->requestRepository->getRequest($requestId);
        $receiverName = $this->userRepository->getManagerLabel($request->manager_id);
        $senderName = $request->user_name;
        $message = 'Hello * '.$receiverName.'* ! You Have A New Leave Request from *'.$senderName.'*';
        return TelegramMessage::create()
            ->to('@test_leave_notification')
            ->content($message);
//            ->document($document);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
