<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use App\Models\LeaveRequest\LeaveRequestRepository;
use App\Models\UserRepository;

class UpdateRequest extends Notification
{
    use Queueable;

    protected LeaveRequestRepository $requestRepository;

    protected UserRepository $userRepository;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        LeaveRequestRepository $leaveRequestRepository,
        UserRepository $userRepository
    )
    {
        $this->requestRepository = $leaveRequestRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ["telegram"];
    }

    public function toTelegram($notifiable)
    {
        $requestId = $notifiable;
        $request = $this->requestRepository->getRequest($requestId);
        $receiverName = $this->userRepository->getManagerLabel($request->manager_id);
        $senderName = $request->user_name;
        $message = '';
        $url = url("/leave-request/index");
        if ($request->status == 'approve') {
            $message = 'Congratulation *'.$senderName.' *! Your leave request has been approved. Click the [link]('.$url.') to view your request';
        } elseif ($request->status == 'decline') {
            $message = 'Hi *'.$senderName.'* ! Your Leave Request Has Been Decline. Please Check Comment From *'.$receiverName.'* Manager For Detail.';
        }
        return TelegramMessage::create()
            ->to('@test_leave_notification')
            ->content($message);
//            ->document($document);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
